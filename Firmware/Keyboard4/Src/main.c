/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "i2c.h"
#include "iwdg.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "usbd_hid.h"
#include "keyboardCodes.h"
#include "inlineDebounce.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

GPIO_TypeDef* gpios[5] = {
		LED0_GPIO_Port,
		LED1_GPIO_Port,
		LED2_GPIO_Port,
		LED3_GPIO_Port,
		LED4_GPIO_Port
};

uint16_t pins[5] = {
		LED0_Pin,
		LED1_Pin,
		LED2_Pin,
		LED3_Pin,
		LED4_Pin
};

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define BTN_DEB 50
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void reenumerate(){
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_12;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//reenumerate
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	for(int i = 0 ; i < 10 ; i++){
		HAL_IWDG_Refresh(&hiwdg);
		HAL_Delay(50);
	}
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_IWDG_Refresh(&hiwdg);
	for(int i = 0 ; i < 10 ; i++){
		HAL_IWDG_Refresh(&hiwdg);
		HAL_Delay(50);
	}
}

void KeyboardWrite(uint8_t special, uint8_t Key0, uint8_t Key1, uint8_t Key2, uint8_t Key3, uint8_t Key4, uint8_t Key5){
    static uint8_t buff[9] = {1, 0, 0, 0, 0, 0, 0, 0, 0}; /* 9 bytes long report */

    buff[1] = special;

    buff[2] = 0x00;

    buff[3] = Key0;
    buff[4] = Key1;
    buff[5] = Key2;
    buff[6] = Key3;
    buff[7] = Key4;
    buff[8] = Key5;

    USBD_HID_SendReport(&hUsbDeviceFS, buff, 9);
    HAL_IWDG_Refresh(&hiwdg);
    HAL_Delay(5);

    for(int i = 1 ; i < 9 ; i++){
        buff[i] = 0x00;
    }

    USBD_HID_SendReport(&hUsbDeviceFS, buff, 9);
    HAL_IWDG_Refresh(&hiwdg);
    HAL_Delay(5);
}

typedef enum {LED_OFF, LED_ON, LED_BLINK} ledState_t;
ledState_t ledState[5] = {LED_BLINK, LED_OFF, LED_OFF, LED_OFF, LED_OFF};
uint32_t ledPeriod[5] = {250, 250, 250, 250, 100};

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	static uint32_t us100Tick = 0;
	us100Tick ++;
	for(int i = 0 ; i < 5 ; i++){
		switch(ledState[i]){
			case LED_OFF:
				HAL_GPIO_WritePin(gpios[i], pins[i], GPIO_PIN_RESET);
				break;
			case LED_ON:
				HAL_GPIO_WritePin(gpios[i], pins[i], GPIO_PIN_SET);
				break;
			case LED_BLINK:
				HAL_GPIO_WritePin(gpios[i], pins[i], ((us100Tick % ledPeriod[i]) < (ledPeriod[i] / 2))?GPIO_PIN_SET:GPIO_PIN_RESET);
				break;
			}
	}
}

typedef struct{
	uint8_t turboMode;
	uint8_t capsLock;
	uint8_t autoEnter;
}config_t;

typedef struct{
	config_t config;
	uint8_t configEdit;
}modeKbd_t;

modeKbd_t mode;

#define MEMORY_TIMEOUT_MS 50
#define MEMORY_BASE_ADDRESS 0xA0
#define MEMORY_MAX_ADDRESS 0xFF

uint16_t divCeli(uint16_t a, uint16_t b) {
    uint16_t value = a / b;
    if (a % b > 0) {
        value += 1;
    }
    return value;
}

void i2cMemoryWriteBlock(uint16_t address, uint16_t dataSize, uint8_t* data) {
    HAL_IWDG_Refresh(&hiwdg);
    for (unsigned int i = 0; i < divCeli(dataSize, 8); i++) {
        HAL_I2C_Mem_Write(&hi2c1,
            MEMORY_BASE_ADDRESS + ((address / 128) & 0b00001110),
            address % 256,
            0x01,
            &data[i * 8],
            dataSize > (i + 1) * 8 ? 8 : dataSize - i * 8,
            MEMORY_TIMEOUT_MS);
        address += 8;
        HAL_IWDG_Refresh(&hiwdg);
        HAL_Delay(5);
    }
}

void i2cMemoryReadBlock(uint16_t address, uint16_t dataSize, uint8_t* data) {
    HAL_IWDG_Refresh(&hiwdg);
    for (unsigned int i = 0; i < divCeli(dataSize, 8); i++) {
        HAL_I2C_Mem_Read(&hi2c1,
            MEMORY_BASE_ADDRESS + ((address / 128) & 0b00001110),
            address % 256,
            0x01,
            &data[i * 8],
            dataSize > (i + 1) * 8 ? 8 : dataSize - i * 8,
            MEMORY_TIMEOUT_MS);
        address += 8;
        HAL_IWDG_Refresh(&hiwdg);
    }
}

bool validateConfig(uint8_t configEncoded){
	// check marker bits
	if((configEncoded & 0b10001000) != 0b10000000){
		return false;
	}

	// check control bits (aka negated settings)
	uint8_t configEncodedCheck = ((~configEncoded)>>4) & 0b00001111;
	configEncoded &= 0b00001111;
	if(configEncoded != configEncodedCheck){
		return false;
	}

	return true;
}

config_t decodeConfig(uint8_t configEncoded){
	config_t configDecoded = {.turboMode = 0, .capsLock = 0, .autoEnter = 0};
	if(configEncoded & (1<<0)) configDecoded.autoEnter = 1;
	if(configEncoded & (1<<1)) configDecoded.capsLock = 1;
	if(configEncoded & (1<<2)) configDecoded.turboMode = 1;

	return configDecoded;
}

uint8_t encodeConfig(config_t configDecoded){
	uint8_t configEncoded = 0;
	if(configDecoded.autoEnter) configEncoded |= (1<<0);
	if(configDecoded.capsLock) configEncoded |= (1<<1);
	if(configDecoded.turboMode) configEncoded |= (1<<2);

	configEncoded |= (~configEncoded)<<4;

	return configEncoded;
}

config_t readConfig(){
	config_t readout = {.turboMode = 0, .capsLock = 0, .autoEnter = 0};
	for(int address = 0 ; address < MEMORY_MAX_ADDRESS ; address++){
		uint8_t configEncoded;
		i2cMemoryReadBlock(address, 1, &configEncoded);
		if(validateConfig(configEncoded)){
			readout = decodeConfig(configEncoded);
			break;
		}
	}
	return readout;
}

void saveConfig(config_t toSave){
	uint8_t configEncoded;

	for(int address = 0 ; address < MEMORY_MAX_ADDRESS ; address++){
		i2cMemoryReadBlock(address, 1, &configEncoded);
		if(validateConfig(configEncoded)){
			configEncoded = 0xFF;
			i2cMemoryWriteBlock(address, 1, &configEncoded); // erease previous setting
			configEncoded = encodeConfig(toSave);
			i2cMemoryWriteBlock((address + 1) % MEMORY_MAX_ADDRESS, 1, &configEncoded); // save actual setting

			return;
		}
	}
	configEncoded = encodeConfig(toSave);
	i2cMemoryWriteBlock(0, 1, &configEncoded); // save actual setting at first location (memory empty)
}

void ledSet(modeKbd_t mode){
	if(mode.config.autoEnter){
		ledState[1] = mode.configEdit?LED_BLINK:LED_ON;
	}else{
		ledState[1] = LED_OFF;
	}
	if(mode.config.capsLock){
		ledState[2] = mode.configEdit?LED_BLINK:LED_ON;
	}else{
		ledState[2] = LED_OFF;
	}
	if(mode.config.turboMode){
		ledState[3] = mode.configEdit?LED_BLINK:LED_ON;
		ledState[4] = mode.configEdit?LED_BLINK:LED_ON;
	}else{
		ledState[3] = LED_OFF;
		ledState[4] = mode.configEdit?LED_BLINK:LED_OFF;
	}
}

bool isShiftPressed(void){
	return !HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin);
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

// Enter
debouncer_t BTN1Debouncer = {.clickMin = BTN_DEB, .holdWait = 500, .holdRep = 50};
// Shift
debouncer_t BTN2Debouncer = {.clickMin = BTN_DEB, .holdWait = 500, .holdRep = 10000};
// D / XD^n
debouncer_t BTN3Debouncer = {.clickMin = BTN_DEB, .holdWait = 500, .holdRep = 50};
// X / XD
debouncer_t BTN4Debouncer = {.clickMin = BTN_DEB, .holdWait = 500, .holdRep = 50};



/* USER CODE END 0 */

int main(void)
{

	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	__HAL_RCC_I2C1_CLK_ENABLE();
	MX_GPIO_Init();
	MX_I2C1_Init();
	//MX_USB_DEVICE_Init();
	MX_TIM1_Init();
	MX_IWDG_Init();

	/* USER CODE BEGIN 2 */
	//HAL_IWDG_Refresh(&hiwdg);
	HAL_IWDG_Start(&hiwdg);
	HAL_IWDG_Refresh(&hiwdg);
	reenumerate();
	HAL_IWDG_Refresh(&hiwdg);
	MX_USB_DEVICE_Init();
	HAL_IWDG_Refresh(&hiwdg);
	HAL_TIM_Base_Start_IT(&htim1);

	HAL_IWDG_Refresh(&hiwdg);

	mode.config = readConfig();
	mode.configEdit = 0;
	ledSet(mode);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
	HAL_IWDG_Refresh(&hiwdg);
	keyState_t state;
	// Enter/auto enter
	state = debounceRepAdv(&BTN1Debouncer, !HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin),HAL_GetTick());
	if(state != KEY_NO){
		//action for BTN1
		if(mode.configEdit == 0){
			if(state == KEY_HOLD && isShiftPressed()){
				mode.configEdit = 1;
				ledSet(mode);
			}else{
				KeyboardWrite(isShiftPressed()?SHIFT_LEFT:0x00, KEY_RETURN, 0x00, 0x00, 0x00, 0, 0);  // [Shift?][Enter]
			}
		}else{
			mode.config.autoEnter = !mode.config.autoEnter;
			ledSet(mode);
		}
	}
	// Shift / caps
	state = debounceRepAdv(&BTN2Debouncer, !HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin),HAL_GetTick());
	if(state == KEY_CLICK){
		//action for BTN2
		if(mode.configEdit == 1){
			mode.config.capsLock = !mode.config.capsLock;
			ledSet(mode);
		}
	}
	// D / XD^n
	state = debounceRepAdv(&BTN3Debouncer, !HAL_GPIO_ReadPin(BTN3_GPIO_Port, BTN3_Pin),HAL_GetTick());
	if(state != KEY_NO){
		//action for BTN3
		if(mode.configEdit == 0){
			bool isShift = isShiftPressed();
			isShift = (isShift && !mode.config.capsLock) || (!isShift && mode.config.capsLock);
			if(mode.config.turboMode){
				KeyboardWrite(isShift?SHIFT_LEFT:0x00, KEY_X, KEY_D, 0x00, 0x00, 0x00, 0x00);  // [Shift?][X][D]
				for(int i = 0 ; i < 83 ; i++){
					KeyboardWrite(isShift?SHIFT_LEFT:0x00, KEY_D, 0x00, 0x00, 0x00, 0x00, 0x00);  // [Shift?][D]
				}
			}else{
				KeyboardWrite(isShift?SHIFT_LEFT:0x00, KEY_D, 0x00, 0x00, 0x00, 0x00, 0x00);  // [Shift?][D]
			}
			if(mode.config.autoEnter) KeyboardWrite(0x00, KEY_RETURN, 0x00, 0x00, 0x00, 0, 0);  // [Enter]
		}else{
			mode.config.turboMode = !mode.config.turboMode;
			ledSet(mode);
		}
	}
	// X / XD
	state = debounceRepAdv(&BTN4Debouncer, !HAL_GPIO_ReadPin(BTN4_GPIO_Port, BTN4_Pin),HAL_GetTick());
	if(state != KEY_NO){
		//action for BTN4
		if(mode.configEdit == 0){
			bool isShift = isShiftPressed();
			isShift = (isShift && !mode.config.capsLock) || (!isShift && mode.config.capsLock);
			if(mode.config.turboMode){
				KeyboardWrite(isShift?SHIFT_LEFT:0x00, KEY_X, KEY_D, 0x00, 0, 0, 0);  // [Shift?][X][D]
			}else{
				KeyboardWrite(isShift?SHIFT_LEFT:0x00, KEY_X, 0x00, 0x00, 0x00, 0x00, 0x00);  // [Shift?][X]
			}
			if(mode.config.autoEnter) KeyboardWrite(0x00, KEY_RETURN, 0x00, 0x00, 0x00, 0, 0);  // [Enter]
		}else{
			mode.configEdit = 0;
			ledSet(mode);
			saveConfig(mode.config);
		}
	}
	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */

	}
	/* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
